class Worker {
  constructor(hoursWorked, rate){
    this.hoursWorked = hoursWorked;
    this.rate = rate;
    this.TAX = 0.05; // nilai pajak tetap 5%
  }

  calculateSalary() {
    return this.hoursWorked * this.rate;
  }

  basicSalary(){
    return this.calculateSalary();
  } 

  overviewSalary() {
    return this.calculateSalary() - ( this.TAX * this.basicSalary() );
  }
}

const worker1 = new Worker(40, 10000)

console.log("Salary awal: ", worker1.calculateSalary() ,'\n','Salary dengan potongan pajak: ', worker1.overviewSalary())

