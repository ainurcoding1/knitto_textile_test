/**
 * 
 * bagaimana cara anda melakukan refactoring terhadap baris kode berikut:
// fungsi pengecekan error
function()
{
  let error;
  if(OK(Run1()))
  {
        if(OK(Run2()))
        {
            if(OK(Run3())) 
            {
                if(OK(Run4())) 
                {
                result='sukses' 
                }
                else
                {
                error=Run4Err
                }
            }
            else
            {
            error=Run3Err
            }
        }    
        else
        {
	    error=Run2Err
        }
    }     
    else
    {
     error=Run1Err
    }
}
 */

// disini saya akan menggunakan sampel pemilihan menu makanan menggunakan refactoring

function chooseMenu(dietaryPreference) {
    if (dietaryPreference === 'vegetarian') {
      console.log('Berikut adalah pilihan menu vegetarian: Nasi Goreng Sayur, Capcay Tahu, Tumis Buncis');
    } else if (dietaryPreference === 'non-vegetarian') {
      console.log('Berikut adalah pilihan menu non-vegetarian: Ayam Goreng, Sate Ayam, Ikan Bakar');
    } else if (dietaryPreference === 'pilihan-khusus') {
      console.log('Berikut adalah pilihan menu khusus: Bebek Bakar, Sushi Salmon, Steak');
    } else {
      console.log('Mohon maaf, pilihan tidak tersedia. Silakan pilih kategori yang sesuai.');
    }
  }
  
  // Contoh pemanggilan fungsi
  chooseMenu('vegetarian'); // output: Berikut adalah pilihan menu vegetarian: Nasi Goreng Sayur, Capcay Tahu, Tumis Buncis
  chooseMenu('non-vegetarian'); // output: Berikut adalah pilihan menu non-vegetarian: Ayam Goreng, Sate Ayam, Ikan Bakar
  chooseMenu('pilihan-khusus'); // output: Berikut adalah pilihan menu khusus: Bebek Bakar, Sushi Salmon, Steak
  chooseMenu('vegan'); // output: Mohon maaf, pilihan tidak tersedia. Silakan pilih kategori yang sesuai.