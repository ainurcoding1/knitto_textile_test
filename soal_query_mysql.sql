-- 1 buatkan query untuk menampilkan data seperti yang tercantum pada soal nomor 1

-- disini saya buat dulu sebuah database dan contoh data yang nanti akan digunakan untuk melakukan pemanggilan kode

show databases;

create database knitto_test;

use knitto_test;

CREATE TABLE data_periode (
  periode VARCHAR(10)
);

INSERT INTO data_periode (periode) VALUES
  ('2022-01'),
  ('2022-03'),
  ('2022-05'),
  ('2022-04'),
  ('2022-02');
  
show tables;

-- syntax sql dibawah merupakan jawaban dari soal 1
SELECT periode FROM data_periode ORDER BY periode ASC;


-- soal 2, 2.	Buatkan store procedure untuk membuat no transaksi contoh no transaksinya :  MH080523003 , penjelasan MH =kode transaksi, 08=tanggal per kode di buat, 05=bulan per kode dibuat, 23= tahun per kode di buat, 003 no urut transaksi perharinya no urut akan selalu naik hingga angka 999:


DROP PROCEDURE IF EXISTS generate_transaction_number;
DELIMITER //

CREATE PROCEDURE generate_transaction_number(IN input_periode VARCHAR(10))
BEGIN
    DECLARE prefix VARCHAR(2) DEFAULT 'MH';
    DECLARE dayPart VARCHAR(2);
    DECLARE monthPart VARCHAR(2);
    DECLARE yearPart VARCHAR(2);
    DECLARE sequence INT;
    DECLARE transactionNumber VARCHAR(12);

    -- Generate date parts
    SET dayPart = DATE_FORMAT(NOW(), '%d');
    SET monthPart = DATE_FORMAT(NOW(), '%m');
    SET yearPart = DATE_FORMAT(NOW(), '%y');

    -- Get the last used sequence number for the current date
    SELECT IFNULL(MAX(SUBSTRING(transaction_number, 9)), 0) + 1 INTO sequence
    FROM data_periode
    WHERE SUBSTRING(transaction_number, 3, 6) = CONCAT(dayPart, monthPart, yearPart);

    -- Generate the transaction number
    SET transactionNumber = CONCAT(prefix, dayPart, monthPart, yearPart, LPAD(sequence, 3, '0'));

    -- Insert the generated transaction number into the table
    INSERT INTO data_periode (periode, transaction_number) VALUES (input_periode, transactionNumber);
END //

DELIMITER ;

SHOW PROCEDURE STATUS WHERE Name = 'generate_transaction_number';
-- procedure di bawah sya modifikasi agar bisa memasukkan periode
CALL generate_transaction_number('2022-02');

 

desc data_periode;
select * from data_periode;

ALTER TABLE data_periode
ADD COLUMN transaction_number VARCHAR(50);

ALTER TABLE data_periode
DROP COLUMN transaction_number;

truncate data_periode;
