const knex = require('knex');
const database = knex({
  client: "mysql",
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    // port: process.env.DB_PORT || "3360",
  },
});

database
  .raw("SELECT 1")
  .then(() => {
    console.log("Database connection successful!");
  })
  .catch((err) => {
    console.error("Error connecting to the database:", err);
  });

module.exports = database;


