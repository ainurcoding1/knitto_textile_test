const express = require('express');
const router = express.Router();
const ResourceController = require('../controllers/resourceController');

router.get('/', (req, res) => {
    res.send('Welcome to api knitto test!');
});

module.exports = router;