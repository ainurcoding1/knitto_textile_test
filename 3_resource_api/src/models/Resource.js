const knex = require('../config/database/connection');


class Resource {
  static getAll() {
    return knex.select('*').from('karyawan').whereNull('is_deleted');
  }

  static getOne(id) {
    return knex.select().from('karyawan').where('id', id).limit(1);
  }

  static create(data) {
    return knex('karyawan').insert(data);
  }

  static update(id, data) {
    return knex('karyawan').where('id', id).update(data);
  }

  static delete(id, data) {
    return knex('karyawan').where('id', id).update(data);
  }
}

module.exports = Resource;