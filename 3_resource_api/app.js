const express = require('express');
const bodyParser = require('body-parser');
const app = express();
require('dotenv').config()

// parse application/json
app.use(bodyParser.json());

// define routes
const ResourceRoutes = require('./src/routes/resourceRoutes');
const homeRoutes = require('./src/routes/homeRoutes');
app.use('/', homeRoutes);
app.use('/resources', ResourceRoutes);

module.exports = app


