const request = require("supertest");
const app = require("../app");

describe("GET /resources", () => {
  let response;

  beforeAll(async () => {
    const port = process.env.PORT || 3000;
    server = app.listen(port, () => {
      console.log(`Server started on port ${port}`);
    });
    response = await request(app).get("/resources");
  });

  afterAll((done) => {
    server.close(done);
  });

  it("Message Success", () => {
    expect(response.body).toHaveProperty("message", "Success!");
  });

  it("should return list of employee", () => {
    expect(response.statusCode).toBe(200);
    expect(response.body).toHaveProperty("result");
  });

  it("should have an id as a number", () => {
    const user = response.body.result[0];
    expect(typeof user.id).toBe('number');
    expect(user.id).toEqual(expect.any(Number));
  });
  it("should have an nama as a string", () => {
    const user = response.body.result[0];
    expect(typeof user.nama).toBe('string');
    expect(user.nama).toEqual(expect.any(String));
  });
  it("should have an gender as a string", () => {
    const user = response.body.result[0];
    expect(typeof user.gender).toBe('string');
    expect(user.gender).toEqual(expect.any(String));
  });
  it("should have an alamat as a string", () => {
    const user = response.body.result[0];
    expect(typeof user.alamat).toBe('string');
    expect(user.alamat).toEqual(expect.any(String));
  });
  it("should have an umur as a number", () => {
    const user = response.body.result[0];
    expect(typeof user.umur).toBe('number');
    expect(user.umur).toEqual(expect.any(Number));
  });
});
